package com.example.myapplication.components

import androidx.compose.material.BottomNavigationItem
import androidx.compose.material.Icon
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import androidx.navigation.NavGraph.Companion.findStartDestination
import androidx.navigation.compose.currentBackStackEntryAsState
import com.example.myapplication.graph.BottomNavItem
import com.example.myapplication.theme.GreyCustom
import com.example.myapplication.theme.Orange

@Composable
fun BottomNavigation(navController: NavController) {
    val items = listOf(
        BottomNavItem.DuckDuckGo,
        BottomNavItem.Google,
    )

    androidx.compose.material.BottomNavigation(
        backgroundColor = GreyCustom,
        contentColor = Color.Blue
    ) {
        val navBackStackEntry by navController.currentBackStackEntryAsState()
        val currentRoute = navBackStackEntry?.destination?.route

        items.forEach { item ->
            BottomNavigationItem(
                icon = {
                    Icon(painterResource(id = item.icon), tint = if (currentRoute == item.route) Orange else Color.Black, contentDescription = item.title) },
                label = {
                    Text(
                        text = item.title,
                        fontSize = if (currentRoute == item.route) 12.sp else 9.sp
                    )
                },
                selectedContentColor = Orange,
                unselectedContentColor = Color.Black,
                alwaysShowLabel = true,
                selected = currentRoute == item.route,
                onClick = {
                    if (currentRoute == item.route) {
                        return@BottomNavigationItem
                    }
                    navController.navigate(item.route) {
                        popUpTo(navController.graph.findStartDestination().id) {
                            saveState = true
                        }

                        launchSingleTop = true
                        restoreState = true
                    }
                }
            )
        }
    }
}