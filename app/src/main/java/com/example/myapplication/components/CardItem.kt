package com.example.myapplication.components

import android.content.Context
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Card
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.myapplication.models.SearchAdapterItem
import com.example.myapplication.utils.IntentHelper


@OptIn(ExperimentalMaterialApi::class)
@Composable
fun CardItem(searchAdapterItem: SearchAdapterItem) {
    val context = LocalContext.current
    Card(
        onClick = {
            openBrowser(context, searchAdapterItem.url)
        },
        modifier = Modifier
            .padding(4.dp)
            .fillMaxWidth()
            .fillMaxHeight()
            .padding(top = 4.dp),
        shape = RoundedCornerShape(8.dp),
        elevation = 2.dp,
        content = {
            Column {
                Spacer(modifier = Modifier.height(4.dp))
                Text(
                    text = searchAdapterItem.title,
                    Modifier.padding(horizontal = 8.dp),
                    fontSize = 14.sp,
                    color = Color.Blue,
                    fontWeight = FontWeight.Bold
                )
                Spacer(modifier = Modifier.height(2.dp))

                Text(
                    text = searchAdapterItem.text,
                    Modifier.padding(horizontal = 8.dp, vertical = 2.dp),
                    fontSize = 14.sp,
                    color = Color.Black
                )

                Spacer(modifier = Modifier.height(4.dp))
            }
        }
    )
}

fun openBrowser(context: Context, url: String) {
    IntentHelper.openUrl(context, url)
}
