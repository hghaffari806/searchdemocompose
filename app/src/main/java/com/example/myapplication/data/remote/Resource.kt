package com.example.search.data.remote

import androidx.annotation.StringRes

sealed class Resource<T>(val data: T? = null, @StringRes val msgId: Int? = null,val message: String? = null) {
    class Success<T>(data: T) : Resource<T>(data)
    class Error<T>(message: String? = null, @StringRes msgId: Int? = null, data: T? = null) : Resource<T>(data, msgId, message)
    class Loading<T>(data: T? = null) : Resource<T>(data)
}