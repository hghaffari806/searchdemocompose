package com.example.myapplication.data.remote.duck

import com.squareup.moshi.Json

data class DuckDuckGoItem(
    @field:Json(name = "FirstURL")
    val firstUrl: String,
    @field:Json(name = "Result")
    val result: String?,
    @field:Json(name = "Text")
    val text: String
)
