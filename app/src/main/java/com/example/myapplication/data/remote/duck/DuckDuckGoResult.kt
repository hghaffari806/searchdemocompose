package com.example.myapplication.data.remote.duck

import com.squareup.moshi.Json

data class DuckDuckGoResult(
    @field:Json(name = "Abstract")
    val abstract: String,
    @field:Json(name = "AbstractSource")
    val abstractSource: String,
    @field:Json(name = "AbstractURL")
    val abstractUrl: String,
    @field:Json(name = "Heading")
    val heading: String,
    @field:Json(name = "Entity")
    val entity: String,
    @field:Json(name = "RelatedTopics")
    val relatedTopics: List<DuckDuckGoItem>,
    @field:Json(name = "Results")
    val results: List<DuckDuckGoItem>
)
