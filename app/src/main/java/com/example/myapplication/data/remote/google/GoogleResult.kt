package com.example.myapplication.data.remote.google

data class GoogleResult(
    val organic_results: List<OrganicResult>
)