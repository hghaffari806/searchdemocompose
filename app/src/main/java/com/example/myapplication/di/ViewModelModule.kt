package com.example.myapplication.di

import com.example.myapplication.data.remote.duck.DuckDuckGoApi
import com.example.myapplication.data.remote.duck.DuckDuckGoResult
import com.example.myapplication.data.remote.google.GoogleApi
import com.example.myapplication.data.remote.google.GoogleResult
import com.example.myapplication.repositories.SearchRepository
import com.example.myapplication.ui.duckduckgo.DuckDuckGoRepositoryImpl
import com.example.myapplication.ui.google.GoogleRepositoryImpl
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent
import dagger.hilt.android.scopes.ViewModelScoped
import retrofit2.Retrofit

@Module
@InstallIn(ViewModelComponent::class)
object ViewModelModule {

    @Provides
    @ViewModelScoped
    fun provideDuckDuckGoApi(retrofit: Retrofit) : DuckDuckGoApi {
        return retrofit.create(DuckDuckGoApi::class.java)
    }

    @Provides
    @ViewModelScoped
    fun provideGoogleApi(retrofit: Retrofit) : GoogleApi {
        return retrofit.create(GoogleApi::class.java)
    }


    @Provides
    @ViewModelScoped
    fun provideDuckDuckGoRepository(duckDuckGoApi: DuckDuckGoApi): SearchRepository<DuckDuckGoResult> {
        return DuckDuckGoRepositoryImpl(
            duckDuckGoApi
        )
    }

    @Provides
    @ViewModelScoped
    fun provideGoogleRepository(googleApi: GoogleApi): SearchRepository<GoogleResult> {
        return GoogleRepositoryImpl(
            googleApi
        )
    }
}