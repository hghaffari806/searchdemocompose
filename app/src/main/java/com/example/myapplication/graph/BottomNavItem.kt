package com.example.myapplication.graph

import com.example.myapplication.R


sealed class BottomNavItem(var title:String, var icon:Int, var route:String){
    object DuckDuckGo : BottomNavItem("DuckDuckGo", R.drawable.ic_baseline_search_24, "home")
    object Google: BottomNavItem("Google", R.drawable.ic_baseline_search_24, "google")
}