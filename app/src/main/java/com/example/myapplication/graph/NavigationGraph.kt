package com.example.myapplication.graph

import androidx.compose.runtime.Composable
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import com.example.myapplication.ui.duckduckgo.DuckDuckGoPage
import com.example.myapplication.ui.duckduckgo.DuckDuckGoViewModel
import com.example.myapplication.ui.google.GooglePage
import com.example.myapplication.ui.google.GoogleViewModel

@ExperimentalComposeUiApi
@Composable
fun NavigationGraph(navController: NavHostController) {
    NavHost(navController, startDestination = BottomNavItem.DuckDuckGo.route) {
        composable(BottomNavItem.DuckDuckGo.route) {
            val viewModel = hiltViewModel<DuckDuckGoViewModel>()
            DuckDuckGoPage(viewModel)
        }
        composable(BottomNavItem.Google.route) {
            val viewModel = hiltViewModel<GoogleViewModel>()
            GooglePage(viewModel)
        }
    }
}