package com.example.myapplication.graph

sealed class Pages(val route: String) {
    object PageMain : Pages("main")
}