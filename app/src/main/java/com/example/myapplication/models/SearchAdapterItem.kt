package com.example.myapplication.models

data class SearchAdapterItem(
    val url: String,
    val title: String,
    val text: String,
    val type: Types,
    val entity: String? = null,
)

enum class Types {
    Heading,
    Item
}