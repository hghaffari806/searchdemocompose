package com.example.myapplication.theme

import androidx.compose.ui.graphics.Color


val Purple200 = Color(0xFFBB86FC)
val Purple500 = Color(0xFF6200EE)
val Background = Color(0xFF0458B5)
val Purple700 = Color(0xFF3700B3)
val Teal200 = Color(0xFF03DAC5)
//val systemBgColor= Color(0xFF3D3D3F)
val systemGray= Color(0xFF8E8E93)
//val systemGray5= Color(0xFF2C2C2E)
val systemGreen= Color(0xFF32D74B)
val systemGreenAlpha = Color(0x5532D74B)
//val fillTertiary = Color(0xFF2C2C30)
//val selectedTab = Color(0x55636366)
val lightCard = Color(0x55A4A4A7)
val blueLabel = Color(0xff0A84FF)
val redLabel = Color(0xffFF2D55)
val Orange = Color(0xFFFF7700)
val grayLabel = Color(0xff8E8E93)

val GreyCustom = Color(0xFFEBEFF3)
val Grey50 = Color(0xFFFAFAFA)
val LightGreen600 = Color(0xFF7cb342)
val Green900 = Color(0xFF1b5e20)
val Yellow700 = Color(0xFFfbc02d)
val Red500 = Color(0xFFf44336)
val Red300 = Color(0xFFe57373)
val Red100 = Color(0xFFffcdd2)
