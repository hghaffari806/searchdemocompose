package com.example.myapplication.ui

import androidx.compose.material.Scaffold
import androidx.compose.runtime.Composable
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.navigation.compose.rememberNavController
import com.example.myapplication.components.BottomNavigation
import com.example.myapplication.graph.NavigationGraph

@OptIn(ExperimentalComposeUiApi::class)
@Composable
fun MainPage() {
    val navController = rememberNavController()
    Scaffold(
        bottomBar = { BottomNavigation(navController = navController) }
    ) { padding ->
        NavigationGraph(navController = navController)
    }
}
