package com.example.myapplication.ui.duckduckgo

import com.example.myapplication.data.remote.duck.DuckDuckGoResult

sealed class DuckDuckGoEvents(
    val duckDuckGoResult: DuckDuckGoResult? = null
) {
    class OnLoading : DuckDuckGoEvents()
    class OnSearchResult(duckDuckGoResult: DuckDuckGoResult) : DuckDuckGoEvents(duckDuckGoResult = duckDuckGoResult)
    class OnError : DuckDuckGoEvents()
}
