package com.example.myapplication.ui.duckduckgo

import com.example.myapplication.data.remote.duck.DuckDuckGoApi
import com.example.myapplication.data.remote.duck.DuckDuckGoResult
import com.example.myapplication.repositories.SearchRepository
import com.example.search.data.remote.Resource
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

class DuckDuckGoRepositoryImpl @Inject constructor(
    private val api: DuckDuckGoApi
) : SearchRepository<DuckDuckGoResult> {

    override suspend fun searchTheQuery(query: String) = flow {
        emit(Resource.Loading())
        val result = api.search(url = "https://api.duckduckgo.com/", query = query)
        if (result.isSuccessful) {
            val body = result.body()!!
            emit(Resource.Success(body))
            return@flow
        }
        emit(Resource.Error())
    }
}