package com.example.myapplication.ui.duckduckgo

import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.myapplication.ui.duckduckgo.DuckDuckGoEvents
import com.example.myapplication.ui.duckduckgo.DuckDuckGoRepositoryImpl
import com.example.search.data.remote.Resource
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class DuckDuckGoViewModel @Inject constructor(
    private val repository: DuckDuckGoRepositoryImpl
) : ViewModel() {

    val query = mutableStateOf("")

    val eventsFlow = MutableStateFlow<DuckDuckGoEvents?>(null)

    fun search(query: String) = viewModelScope.launch(IO) {
        repository.searchTheQuery(query).collect{
            when(it) {
                is Resource.Loading -> eventsFlow.emit(DuckDuckGoEvents.OnLoading())

                is Resource.Success -> eventsFlow.emit(DuckDuckGoEvents.OnSearchResult(it.data!!))

                is Resource.Error -> eventsFlow.emit(DuckDuckGoEvents.OnError())
            }
        }
    }

    fun onQueryChanged(query: String){
        this.query.value = query
    }
}