package com.example.myapplication.ui.google

import androidx.appcompat.app.AppCompatActivity
import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.Icon
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.material.TextField
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Search
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.mutableStateListOf
import androidx.compose.runtime.remember
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalSoftwareKeyboardController
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.unit.dp
import com.example.myapplication.R
import com.example.myapplication.components.CardItem
import com.example.myapplication.models.SearchAdapterItem
import com.example.myapplication.theme.grayLabel
import com.example.myapplication.utils.ItemConverter
import com.example.myapplication.utils.extensions.showToast

@OptIn(ExperimentalAnimationApi::class, ExperimentalComposeUiApi::class)
@Composable
fun GooglePage(viewModel: GoogleViewModel) {
    val activity = LocalContext.current as AppCompatActivity
    val searchResultList = remember {
        mutableStateListOf<SearchAdapterItem>()
    }

    LaunchedEffect(Unit) {
        viewModel.eventFlow.collect {
            when (it) {
                is GoogleEvents.OnLoading -> {}

                is GoogleEvents.OnSearchResult -> {
                    val results = it.googleResult?.organic_results?.let { it1 ->
                        ItemConverter.convertGoogleResultToAdapterItem(
                            it1
                        )
                    }

                    results?.let { it1 ->
                        searchResultList.clear()
                        searchResultList.addAll(it1)
                    }
                }

                is GoogleEvents.OnError -> activity.showToast(R.string.something_went_wrong)
                else -> {}
            }
        }
    }

    val query = viewModel.query.value
    val softKeyboardController = LocalSoftwareKeyboardController.current

    Column {
        Surface(
            modifier = Modifier
                .padding(8.dp)
                .fillMaxWidth(),
            shape = RoundedCornerShape(4.dp),
            color = Color.Blue,
            elevation = 8.dp,
        ) {
            Row(modifier = Modifier.fillMaxWidth()) {
                TextField(
                    value = query,
                    onValueChange = {
                        viewModel.onQueryChanged(it)
                    },
                    modifier = Modifier
                        .fillMaxWidth()
                        .background(Color.Blue),
                    label = {
                        Text(text = "Search", color = grayLabel)
                    },
                    keyboardOptions = KeyboardOptions(
                        keyboardType = KeyboardType.Text,
                        imeAction = ImeAction.Done,
                    ),
                    keyboardActions = KeyboardActions(
                        onDone = {
                            viewModel.search(query)
                            softKeyboardController?.hide()
                        }
                    ),
                    leadingIcon = {
                        Icon(Icons.Filled.Search, "Search", tint = Color.White)
                    },
                    textStyle = TextStyle(color = Color.White),
                )
            }
        }

        LazyColumn(Modifier.padding(start = 4.dp, end = 4.dp, bottom = 60.dp)) {
            itemsIndexed(
                items = searchResultList
            ) { _, recipe ->
                CardItem(recipe)
            }
        }
    }
}


