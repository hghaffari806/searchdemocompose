package com.example.myapplication.ui.google

import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.myapplication.ui.duckduckgo.DuckDuckGoEvents
import com.example.myapplication.ui.google.GoogleEvents
import com.example.myapplication.ui.google.GoogleRepositoryImpl
import com.example.search.data.remote.Resource
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class GoogleViewModel @Inject constructor(
    private val repository: GoogleRepositoryImpl
) : ViewModel() {

    val query = mutableStateOf("")

    val eventFlow = MutableStateFlow<GoogleEvents?>(null)

    fun search(query: String) = viewModelScope.launch(Dispatchers.IO) {
        repository.searchTheQuery(query).collect{
            when(it) {
                is Resource.Loading -> eventFlow.emit(GoogleEvents.OnLoading())

                is Resource.Success -> eventFlow.emit(GoogleEvents.OnSearchResult(it.data!!))

                is Resource.Error -> eventFlow.emit(GoogleEvents.OnError())
            }
        }
    }

    fun onQueryChanged(query: String){
        this.query.value = query
    }
}