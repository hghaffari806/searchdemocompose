package com.example.myapplication.utils

import com.example.myapplication.data.remote.duck.DuckDuckGoItem
import com.example.myapplication.data.remote.duck.DuckDuckGoResult
import com.example.myapplication.data.remote.google.OrganicResult
import com.example.myapplication.models.SearchAdapterItem
import com.example.myapplication.models.Types
import com.example.myapplication.utils.extensions.extractTitle

object ItemConverter {

    fun convertToAdapterItem(items: List<DuckDuckGoItem>): List<SearchAdapterItem> {
        return items.filter { it.result != null}.map {
            SearchAdapterItem(
                it.firstUrl,
                it.result!!.extractTitle(),
                it.text,
                Types.Item
            )
        }
    }

    fun convertToAdapterItem(result: DuckDuckGoResult): SearchAdapterItem? {
        if (result.abstract.isNotEmpty() && result.abstractUrl.isNotEmpty()) {
            return SearchAdapterItem(
                result.abstractUrl,
                result.heading,
                result.abstract,
                Types.Heading,
                result.entity
            )
        }
        return null
    }

    fun convertGoogleResultToAdapterItem(items: List<OrganicResult>): List<SearchAdapterItem> {
        return items.map {
            SearchAdapterItem(
                it.link,
                it.title,
                it.about_page_link,
                Types.Item
            )
        }
    }

}