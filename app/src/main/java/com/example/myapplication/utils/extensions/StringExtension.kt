package com.example.myapplication.utils.extensions

import java.util.regex.Pattern

fun String.extractTitle(): String {
    return try {
        val pattern = Pattern.compile(">(.+?)</a>", Pattern.DOTALL)
        val matcher = pattern.matcher(this)
        matcher.find()
        matcher.group(1) ?: ""
    }catch (e: Exception) {
        ""
    }
}